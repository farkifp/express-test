function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}


/**
 * 
 * @param {string} email Users email
 * @param {string} text Body of the mail
 */
function sendMail(email, text) {
  return wait(500).then(() => {
    console.log(`mail sent to ${email} with text: '${text}'`);
    
    return true;
  });
}

module.exports = {
  sendMail: sendMail
}