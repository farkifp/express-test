const express = require('express');
const userController = require('./user-controller');

const server = express();
const port = 4000;

server.use('/users', userController);

server.listen(port, () => {
  console.log(`Server listening at ${port}`);
});