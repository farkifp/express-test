const express = require('express');
const bodyParser = require('body-parser');
const userRepo = require('./user-repo');
const mailHelper = require('./mail-helper');

const jsonParser = bodyParser.json();

let router = express.Router();

// A mail-helper.js segítségével kiküld egy mailt az adott id-jű user címére a tegnapi eredményeiről. Email szövege (text) legyen ez:
//
// "Kedves [Keresztnév]! 
//
// Tegnap [lépések száma] lépést tettél meg, és [emeletek száma] emeletet küzdöttél le."
// Az adatokat a user-repo.js (aszinkron) függvényeivel tudod 'lekérni'. Ahol lehet, és van értelme, párhuzamosan kérd le az adatokat, hogy
// minél gyorsabban lefuthasson. Ha a user nem létezik, nyilván ne küldjön emailt :)
router.post("/:userId/sendDailyMail", jsonParser, (req, res) => {

});

// Update-eli egy user napi lépsszámát. A body-ban egy json-t várjon, ilyen formátummal: 
// {
// 	"steps": 234,
// 	"date": "2019-10-10"
// }
// A dátum egy string legyen YYYY-MM-DD formátummal, a lépés pozitív egész, a hivatkozott user létezzen. A mentést a user-repo updateUserSteps
// függvénye végzi, ezt hívd meg!
router.put("/:userId/steps", jsonParser, (req, res) => {

});


module.exports = router;

