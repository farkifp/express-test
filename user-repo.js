function wait(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}


/**
 * Returns true is a user with that ID exists
 * @param {number} userId
 */
function userIsExists(userId) {
  return wait(500).then(() => {
    return userId <= 10 ? true : false;
  });
}

/**
 * Returns a user profile if exists, null otherwise
 * @param {number} userId 
 */
function getUserProfile(userId) {
  return wait(600).then(() => {
    return userId <= 10 ? {firstName: 'John', lastName: 'Smith', email: 'john@smith.com'} : null;
  });
}

/**
 * Returns a users step count for the previous day if there's any, null otherwise
 * @param {number} userId 
 */
function getUserStepsForYesterday(userId) {
  return wait(1000).then(() => {
    return userId <= 10 ? 4167 : null;
  });
}

/**
 * Returns a users floor count for the previous day if there's any, null otherwise
 * @param {number} userId 
 */
function getUserFloorsForYesterday(userId) {
  return wait(1000).then(() => {
    return userId <= 10 ? 3 : null;
  });
}

/**
 * Updates a users stepcount for that day. User must exists and date has to be in correct format or will throw an error
 * @param {number} userId 
 * @param {string} date Date in YYYY-MM-DD format
 * @param {number} steps number of steps for the date
 */
function updateUserSteps(userId, date, steps) {
  return wait(1000).then(() => {
    console.log(`Steps update for user [${userId}]: ${date} - ${steps}`)
    return;
  });
}

module.exports = {
  userIsExists:  userIsExists,
  getUserStepsForYesterday:  getUserStepsForYesterday,
  getUserFloorsForYesterday:  getUserFloorsForYesterday,
  updateUserSteps:  updateUserSteps,
  getUserProfile: getUserProfile,
}